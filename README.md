# store-showcase
![Alt Text](https://media.giphy.com/media/XExZPQnnITI1DZnaYd/giphy.gif)


p.s.: Я вставил для превью такие изображение, чтоб была верста гармоничней, в самом проекте используются изображения полученные из запроса. 
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
