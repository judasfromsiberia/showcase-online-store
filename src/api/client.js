import axios from "axios";

async function client() {
  const url = "https://run.mocky.io/v3/0fb7ba7e-5158-4e4f-bfc8-28320bc01cdc";
  return await axios
    .get(url)
    .then(response => response)
    .catch(error => error);
}
export { client };
